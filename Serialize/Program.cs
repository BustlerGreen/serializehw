﻿using System;
using System.Reflection;
using System.Text.Json;
using System.Text;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.ComponentModel;
using System.Diagnostics;

var f = new F();
var ss = new CsvSer<F>();
//call private method to init private fields
var mi = f.GetType().GetMethod("Get", BindingFlags.NonPublic | BindingFlags.Instance);
f = (F)mi.Invoke(f, new object[] { });
var SerObj = ss.Serialize(f);

var sw = Stopwatch.StartNew();
for (int i = 0; i < 10000; i++) ss.Serialize(f);
sw.Stop();
Console.WriteLine($"serialization, us - {(sw.Elapsed.TotalMicroseconds / 10000).ToString()}") ;

sw = Stopwatch.StartNew();
for (int i = 0; i < 10000; i++) f = ss.Deserialize(SerObj);
sw.Stop();
Console.WriteLine($"deserialization, us - {(sw.Elapsed.TotalMicroseconds / 10000).ToString()}");

var stream = new FileStream("f.csv", FileMode.Create, FileAccess.Write);
ss.StoreToCsv(stream, SerObj);
stream = new FileStream("f.csv", FileMode.Open, FileAccess.Read);
f = ss.RestoreFromCsv(stream);

sw = Stopwatch.StartNew();
for (int i = 0; i < 10000; i++) SerObj = JsonSerializer.Serialize(f);
sw.Stop();
Console.WriteLine($"JSON serialization, us - {(sw.Elapsed.TotalMicroseconds / 10000).ToString()}");

sw = Stopwatch.StartNew();
for (int i = 0; i < 10000; i++) f = JsonSerializer.Deserialize<F>(SerObj);
sw.Stop();
Console.WriteLine($"JSON deserialization, us - {(sw.Elapsed.TotalMicroseconds / 10000).ToString()}");


class CsvSer<T> where T: class, new ()
{
    List<FieldInfo> _fields = new List<FieldInfo> ();
    public CsvSer()
    {
        _fields = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Instance).ToList();
    }
    public string Serialize(T t)
    {
        var sb = new StringBuilder();
        var vals = new List<String> ();
        //fields names
        sb.AppendLine(string.Join (",", _fields.Select(x=>x.Name).ToList()));
        //values
        foreach(var f in _fields)
        {
            var fval = f.GetValue(t);
            vals.Add(fval == null ? "" : fval.ToString());
        }
        sb.AppendLine(string.Join(",", vals));
        return sb.ToString();
    }

    public void StoreToCsv(Stream stream, string serObj)
    {
        var sw = new StreamWriter(stream);
        sw.Write(serObj);
        sw.Close();
    }

    public T RestoreFromCsv(Stream stream)
    {
        string SerObj = "";
        using (var sr = new StreamReader(stream))
        {
            SerObj = sr.ReadToEnd();
        }
        return Deserialize(SerObj);
    }

    public T Deserialize(string SerObj)
    {
        string[] names;
        string[] vals; 
        var t = new T();
        string[] nv = SerObj.Split("\n");
        names = nv[0].TrimEnd('\r').Split(",");
        vals = nv[1].TrimEnd('\r').Split(",");

        for (int n = 0; n < names.Length; n++)
        { 
            var f = _fields.First(x=>x.Name == names[n]);
            var conv = TypeDescriptor.GetConverter(f.FieldType);
            var val = conv.ConvertFrom(vals[n]);
            f.SetValue(t, val);
        }
        return t;
     }
}

class F { int i1, i2, i3, i4, i5; F Get() => new F() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5 }; }


